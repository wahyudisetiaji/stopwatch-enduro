package com.example.stopwatchenduro.stopwatch.fragment

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.example.stopwatchenduro.R
import com.example.stopwatchenduro.databinding.FragmentTimeBinding
import com.example.stopwatchenduro.helpers.getPassedSeconds
import com.example.stopwatchenduro.stopwatch.StopwatchActivity
import org.joda.time.DateTime
import java.util.*

class Time : Fragment() {

    private lateinit var binding: FragmentTimeBinding
    private lateinit var navController: NavController
    private val nextAnimationFragment: NavOptions = NavOptions.Builder()
        .setEnterAnim(R.anim.activity_beta_open)
        .setExitAnim(R.anim.activity_alpha_close)
        .setPopEnterAnim(R.anim.activity_beta_open)
        .setPopExitAnim(R.anim.activity_alpha_close)
        .build()

    private val ONE_SECOND = 1000L
    private var passedSeconds = 0
    private var calendar = Calendar.getInstance()
    private val updateHandler = Handler()
    private var beepInterval: MediaPlayer? = null
    private var beepReady: MediaPlayer? = null
    private var beepStart: MediaPlayer? = null
    private var playBeep: Boolean = true
    private var interval: Int = 0
    private var prepare: Int = 0
    private var ready: Int = 0

    private fun parent() = activity as StopwatchActivity
    private fun dataStopwatch() = parent().dataStopwatch

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentTimeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setAudio()
        setObservable()
    }

    override fun onStart() {
        super.onStart()
        playBeep = true
    }

    override fun onResume() {
        super.onResume()
        parent().decorView()
        setupDateTime()
    }

    override fun onStop() {
        super.onStop()
        playBeep = false
    }

    private fun setupDateTime() {
        calendar = Calendar.getInstance()
        passedSeconds = getPassedSeconds()
        updateCurrentTime()
        setLayoutListener()
        setObservable()
    }

    private fun updateCurrentTime() {
        updateHandler.postDelayed({
            passedSeconds++
            updateCurrentTime()
            val intervalSeconds = DateTime().secondOfDay
//            Log.e("Time", "passedSeconds =========== $passedSeconds")
//            Log.e("Time", "intervalSeconds =========== $intervalSeconds")
            if (playBeep) {
                if (isInterval(intervalSeconds)) beepInterval?.start()
            }
        }, ONE_SECOND)
    }

    private fun setLayoutListener() {
        binding.apply {
            config.setOnClickListener {
                playBeep = false
                beepReady?.stop()
                navController.navigate(R.id.time_to_setting, null, nextAnimationFragment)
            }
        }
    }

    private fun setObservable() {
        dataStopwatch().interval.observe(viewLifecycleOwner) {
            interval = it
        }
        dataStopwatch().prepare.observe(viewLifecycleOwner) {
            prepare = it
        }
        dataStopwatch().ready.observe(viewLifecycleOwner) {
            ready = it
        }
        dataStopwatch().startingList.forEach {
            Log.e("startingList", "TIME ========= ${it.raceNumber} ${it.name} ${it.team}")
        }
    }

    private fun setAudio() {
        beepInterval = MediaPlayer.create(requireContext(), R.raw.beep_interval)
        beepReady = MediaPlayer.create(requireContext(), R.raw.beep_ready)
        beepStart = MediaPlayer.create(requireContext(), R.raw.beep_start)
    }

    private fun isInterval(num: Int): Boolean {
        return (num + 1) % interval == 0
    }

}