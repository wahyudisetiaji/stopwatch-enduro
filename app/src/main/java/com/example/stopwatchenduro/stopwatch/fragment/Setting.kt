package com.example.stopwatchenduro.stopwatch.fragment

import android.os.Bundle
import android.renderscript.ScriptGroup.Input
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import com.example.stopwatchenduro.R
import com.example.stopwatchenduro.databinding.FrgamentSettingBinding
import com.example.stopwatchenduro.model.DataStartingList
import com.example.stopwatchenduro.stopwatch.StopwatchActivity
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.joda.time.DateTime
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

class Setting : Fragment() {

    private lateinit var binding: FrgamentSettingBinding
    private lateinit var navController: NavController
    private val backAnimationFragment: NavOptions = NavOptions.Builder()
        .setEnterAnim(R.anim.navigation_alpha_open)
        .setExitAnim(R.anim.navigation_beta_close)
        .setPopEnterAnim(R.anim.navigation_alpha_open)
        .setPopExitAnim(R.anim.navigation_beta_close)
        .build()

    private fun parent() = activity as StopwatchActivity
    private fun dataStopwatch() = parent().dataStopwatch

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FrgamentSettingBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setLayoutListener()
    }

    private fun setLayoutListener() {
        binding.apply {
            back.setOnClickListener {
                navController.navigate(R.id.setting_to_time, null, backAnimationFragment)
            }
            selectFile.setOnClickListener {
                readCSVFile()
            }
        }
    }

    private fun readCSVFile() {
        val inputStream = requireContext().assets.open("startingList.csv").reader()
        val bufferReader = BufferedReader(inputStream)
        val csvParser = CSVParser.parse(
            bufferReader,
            CSVFormat.DEFAULT
        )
        val startingList = mutableListOf<DataStartingList>()
        csvParser.forEachIndexed { index, csvRecord ->
            if (index != 0) {
                val data = csvRecord.get(0).split(";")
                Log.e("startingList", "$data")
                val participant =  DataStartingList(
                    raceNumber = data[0].toInt(),
                    name = data[3],
                    team = data[5],
                    country = data[6],
                    courseClass = data[7],
                    startTime = data[8],
                    startTimePreference = data[9],
                    nonCompetitive = data[11] == "Y",
                )
                startingList.add(participant)
            }
        }
        dataStopwatch().startingList = startingList
        val update = DateTime().millis
        dataStopwatch().update.value = update

    }
}