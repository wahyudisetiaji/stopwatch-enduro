package com.example.stopwatchenduro.model

data class DataStartingList(
    var raceNumber : Int = 0,
    var name : String = "",
    var team : String = "",
    var country : String = "",
    var courseClass : String = "",
    var startTime : String = "",
    var startTimePreference : String = "",
    var nonCompetitive : Boolean = false,
    var startStatus : Boolean = false,
)
