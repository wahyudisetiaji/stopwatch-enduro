package com.example.stopwatchenduro.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

data class DataStopwatch(
    var interval: MutableLiveData<Int> = MutableLiveData(30),
    var prepare: MutableLiveData<Int> = MutableLiveData(10),
    var ready: MutableLiveData<Int> = MutableLiveData(5),
    var update: MutableLiveData<Long> = MutableLiveData(0L),
    var startingList: MutableList<DataStartingList> = mutableListOf()
) : ViewModel()
